import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { CodiceFiscaleComponent } from './codice-fiscale/codice-fiscale.component';
import {CodiceService} from './services/codice.service';

@NgModule({
  declarations: [
    AppComponent,
    CodiceFiscaleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [CodiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
