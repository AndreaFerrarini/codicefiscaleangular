/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CodiceService } from './codice.service';

describe('CodiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CodiceService]
    });
  });

  it('should ...', inject([CodiceService], (service: CodiceService) => {
    expect(service).toBeTruthy();
  }));
});
