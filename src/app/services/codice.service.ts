import { Injectable } from '@angular/core';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import {Codice} from '../codice-fiscale/codice';
import {Http} from '@angular/http';

@Injectable()
export class CodiceService {

  constructor(private http: Http) {
  }
  getCodice(): Observable<Codice> {
    return this.http
      .get('http://localhost:8080/findCodice')
      .map((responseData) => responseData.json());
  }
  addCodice(codice: Codice) {
    return this.http
      .post('http://localhost:8080/addCodice', codice)
      .map((responseData) => responseData.json());
  }

}
