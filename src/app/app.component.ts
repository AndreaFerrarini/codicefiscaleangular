import { Component } from '@angular/core';
import {CodiceService} from './services/codice.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [CodiceService]
})
export class AppComponent {
  title = 'app works!';
  codice;
}
