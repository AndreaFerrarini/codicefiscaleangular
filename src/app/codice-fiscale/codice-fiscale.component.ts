import {Component, OnInit} from '@angular/core';
import {Codice} from './codice';
import {CodiceService} from '../services/codice.service';


@Component({
  selector: 'app-codice-fiscale',
  templateUrl: './codice-fiscale.component.html',
  styleUrls: ['./codice-fiscale.component.css']
})
export class CodiceFiscaleComponent implements OnInit {
  codice = new Codice;

  constructor(private codiceService: CodiceService) {
  }

  generaCodice() {
    this.codiceService.getCodice()
      .subscribe(result => {
        this.codice = result;
        console.log(result);
      }, err => {
        console.log(err);
      });
  }
  aggiungiCodice() {
    this.codiceService.addCodice(this.codice)
      .subscribe(result => {
        console.log(result);
      }, err => {
        console.log(err);
      });
  }
  ngOnInit() {
  }

}
